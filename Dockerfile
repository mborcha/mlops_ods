FROM python:3.11-slim

RUN pip install pdm

WORKDIR /app

RUN pdm init --non-interactive

COPY pyproject.toml pdm.lock ./

RUN pdm install --prod

COPY . .

CMD ["pdm", "run", "start"]
